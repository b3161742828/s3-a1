package com.zuitt.example;

import java.util.Scanner;


public class Factorial {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        try {

            System.out.println("Enter an integer whose factorial will be computed: ");
            int number = in.nextInt();
            int fact = 1;
            int x = 1;
            if(number > 0) {
                System.out.print("The factorial of " + number + " is ");
                while(x <=number){
                    fact=fact*x;
                    x++;
                }
                System.out.println(fact);
            } else if (number == 0) {
                System.out.println("The factorial of " + number + " is " + 1);
            } else {
                System.out.println("Factorial is only for non-negative integer");
            }

            System.out.println("Enter an integer whose factorial will be computed: ");
            number = in.nextInt();
            fact = 1;

            if(number > 0) {
                for(int i=1; i <=number; i++) {
                    fact=fact*i;
                }
                System.out.println("The factorial of " + number + " is " + fact);
            } else if (number == 0) {
                System.out.println("The factorial of " + number + " is " + 1);
            } else {
                System.out.println("Factorial is only for non-negative integer");
            }

            System.out.println("Enter how many layers of pyramid do you want: (+int only)");
            number = in.nextInt();
            if(number > 0){
                for(int i = 1; i <= number; i++) {
                    for(int j = i; j > 0; j--){
                        System.out.print("* ");
                    }
                    System.out.println("");
                }
            } else {
                System.out.println("Please input any positive integer only");
            }
        } catch (Exception e){
            System.out.println("Something went wrong!");
            e.printStackTrace();
        }
    }
}
